import React, { Component } from 'react';
import ReactDOM from 'react-dom';
const render = ReactDOM.render;

class Table extends Component {
 
var NewComponent = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Shopping List</h1>
        <p id="first">Get it done today</p>
        <input id="userinput" type="text" placeholder="user name" />
        <button id="enter">Enter</button>
        <ul>
          <li className="bold red" random={23}>Notebook</li>
          <li>Jello</li>
          <li>Spinach</li>
          <li>Rice</li>
          <li>Birthday Cake</li>
          <li>Candles</li>
        </ul>
      </div>
    );
  }
});

};
          
export default Table;
