import React, { Component } from 'react';
import Table from "./components/Table";
import Input from "./components/Input";
import './App.css';

class App extends Component {
  state = {
    
  };

  onChange = updatedValue => {
    this.setState({
      fields: {
        ...this.state.fields,
        ...updatedValue
      }
    });
  };

render() {
    return (
      <div className="App">
        
      
        <Input onChange={fields => this.onChange(fields)}/>
                
        
        
      </div>
    );
  }
}
export default App;
